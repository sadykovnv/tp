import unittest
from equation.oop import Linear_equation, Quadratic_equation
from equation.func import linear_equation, quadratic_equation


class EquationTest(unittest.TestCase):
    def test_linear_func(self):
        self.assertEqual(linear_equation(1, 4), 4)
        self.assertEqual(linear_equation(2, 4), 2)
        self.assertEqual(linear_equation(2, 3), 1.5)
        self.assertEqual(linear_equation(0, 4), False)

    def test_quadratic_func(self):
        self.assertEqual(quadratic_equation(2, -3, 1), (1, 0.5))
        self.assertEqual(quadratic_equation(2, -3, 6), False)

    def test_linear_oop(self):
        self.assertEqual(Linear_equation(1, 4).__call__(), 4)
        self.assertEqual(Linear_equation(2, 4).__call__(), 2)
        self.assertEqual(Linear_equation(2, 3).__call__(), 1.5)
        self.assertEqual(Linear_equation(0, 4).__call__(), False)

    def test_quadratic_oop(self):
        self.assertEqual(Quadratic_equation(2, -3, 1).__call__(), (1, 0.5))
        self.assertEqual(Quadratic_equation(2, -3, 6).__call__(), False)


if __name__ == '__main__':
    unittest.main()
