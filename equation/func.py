def linear_equation(b, c):
    if b != 0:
        return c / b
    else:
        return False


def quadratic_equation(a, b, c):
    import math
    discr = b ** 2 - 4 * a * c
    if discr > 0:
        x1 = (-b + math.sqrt(discr)) / (2 * a)
        x2 = (-b - math.sqrt(discr)) / (2 * a)
        return (x1, x2)
    elif discr == 0:
        x1 = -b / (2 * a)
        return x1
    elif discr < 0:
        return False