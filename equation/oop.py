class Equation():
    def __repr__(self):
        if hasattr(self, 'x2'):
            return ("x1 = %.2f \nx2 = %.2f" % (self.x1, self.x2))
        elif hasattr(self, 'x1'):
            return ("x1 = %.2f" % self.x1)
        else:
            return ("Корней нет")

    def __call__(self, *args, **kwargs):
        if hasattr(self, 'x2'):
            return (self.x1, self.x2)
        elif hasattr(self, 'x1'):
            return self.x1
        else:
            return False


class Linear_equation(Equation):
    def __init__(self, b, c):
        if b != 0:
            self.x1 = c / b


class Quadratic_equation(Linear_equation):
    def __init__(self, a, b, c):
        import math
        self.discr = b ** 2 - 4 * a * c
        if self.discr > 0:
            self.x1 = (-b + math.sqrt(self.discr)) / (2 * a)
            self.x2 = (-b - math.sqrt(self.discr)) / (2 * a)
        elif self.discr == 0:
            self.x1 = -b / (2 * a)
