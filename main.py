from equation.oop import Linear_equation, Quadratic_equation
from equation.func import linear_equation, quadratic_equation

print("Введите коэффициенты для квадратного уравнения (ax^2 + bx + c = 0):")
a = float(input("a = "))
b = float(input("b = "))
c = float(input("c = "))

# Процедурное
print('Процедурное')
if a == 0:
    x1 = linear_equation(b, c)
else:
    x1 = quadratic_equation(a, b, c)

if type(x1) == type(tuple()):
    print("x1 = %.2f \nx2 = %.2f" % (x1))
elif 'x1' in locals():
    if x1:
        print("x1 = %.2f" % x1)
    else:
        print("Корней нет")
else:
    print("Корней нет")

# ООП
print('ООП')
if a == 0:
    x1 = Linear_equation(b, c)
else:
    x1 = Quadratic_equation(a, b, c)

print(x1)
